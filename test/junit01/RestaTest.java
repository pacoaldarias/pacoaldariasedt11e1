/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junit01;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class RestaTest {

   private Resta resta;

   @Test
   public void aVerSiDecrementaBien() {
      assertEquals(" Test decrementa ", 1.0, resta.decrementa(2.0), 1e-6);
   }

   @Test
   public void aVerSiRestaBien() {
      assertEquals(" Test resta ", 1.0, resta.getDiferencia(2.0, 1.0), 1e-6);
   }

   @Before
   public void paraEjecutarAntes() throws Exception {
      resta = new Resta();
   }

   @After
   public void paraEjecutarDespues() throws Exception {
      // Liberar recursos
   }

}
