/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junit01;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class SumaTest {

   private Suma suma;

   @Test
   public void aVerSiIncrementaBien() {
      assertEquals(" Test incrementa ", 2.0, suma.incrementa(1.0), 1e-6);
   }

   @Test
   public void aVerSiSumaBien() {
      assertEquals(" Test suma ", 2.0, suma.getSuma(1.0, 1.0), 1e-6);
   }

   @Before
   public void paraEjecutarAntes() throws Exception {
      suma = new Suma();
   }

   @After
   public void paraEjecutarDespues() throws Exception {
      // Fin de test . Aqui liberar recursos o borrar rastros del test
   }

}
