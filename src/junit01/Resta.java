/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junit01;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Resta {

   public double decrementa(double a) {
      return a - 1;
   }

   public double getDiferencia(double a, double b) {
      return a - b;
   }

}
